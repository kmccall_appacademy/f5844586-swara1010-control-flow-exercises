# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.delete(str.downcase)

end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  result = ""
  if str.length.odd?
    result << str[str.length / 2]
  else
    result << (str[(str.length / 2)-1] + str[str.length / 2])
  end
  result
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char do |char|
    if VOWELS.include?(char)
      count += 1
    end
  end
    count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  result = []
  (1..num).each do |el|
      result << el
  end
  result.reduce(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ""
  arr.each do |char|
    if separator != "" && char != arr[-1]
     result << char + separator
   else
     result << char
   end
  end
  result
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
   result = []
    str.chars.each_with_index do |char, idx|
      if idx.even?
       result << char.downcase
      else
       result << char.upcase
      end
    end
    result.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  result = []
  str_split = str.split
  str_split.each do |word|
    if word.length >= 5
     result << word.reverse
   else
     result << word
    end
  end
  result.join(" ")

end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []
  (1..n).each do |el|
    if el % 15 == 0
      result << el = "fizzbuzz"
    elsif el % 3 == 0
     result << el = "fizz"
     elsif el % 5 == 0
     result << el = "buzz"
   else
     result << el
    end
  end
  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  result = []
  count = 0
  while count <= arr.length
    arr_pop = arr.pop
    result << arr_pop
    count = 0
   count += 1
end
  result
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num < 2
  (2..num/2).each do |el|
    if num % el == 0
      return false
    end
  end
  true

end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []
  (1..num).each do |el|
    if num % el == 0
      result << el
    end
  end
  result.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  result = []
  num_factors = factors(num)
  num_factors.each do |el|
    if prime?(el)
      result << el
    end
  end
  result.sort
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
   prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  result = []
  result1 = []
  arr.each do |el|
    if el.odd?
      result << el
    else
      result1 << el
    end
  end
  if result1.length > result.length
    result.join.to_i
  else
    result1.join.to_i
  end
end
